﻿using System;

namespace Day14
{
    class Program
    {
        public static int maxDif;
        static void Main(string[] args)
        {
            int[] n = new int[3]{1, 2, 5};
            System.Console.WriteLine(computeDifference(n)); 
        }

        public static int computeDifference(int[] n){
            maxDif = 0;
            for (int i = 0; i < n.Length - 1; i++)
            {
                for (int j = i + 1; j < n.Length; j++)
                {
                    if(Math.Abs(n[i] - n[j]) > maxDif) {
                        maxDif = Math.Abs(n[i] - n[j]);
                    }
                }
            }
            return maxDif;
        }
    }
}
